#   Version 1
#   Calcular si un numero es primo o no
#   autor   =   "Michael Vasquez"
#   email   =   "michael.vasquez@unl.edu.ec"


def esprimo(n):
    contador = 0
    for i in range (1, n+1):
        if n % i == 0:
            contador += 1
            print("Los numeros a los que es divisible es: ", i)

    if contador == 2:
        print("El numero", n, "si es primo")
        return True
    else:
        print("El numero", n, "no es primo")
        return False

try:
    a = int(input("Ingresa el numero que deseas saber si es primo o no:\t"))
    if a > 0:
        print(esprimo(a))
    else:
        print("Ingrese un numero positivo")

except ValueError:
    print("ERROR\nIngrese solo numeros positivos enteros")
