#   Version 3
#   Calcular si un numero es primo o no
#   autor   =   "Michael Vasquez"
#   email   =   "michael.vasquez@unl.edu.ec"

try:
    n = int(input("Ingrese un numero entero positivo"))
    if n > 0:
        for i in range(2,n):
            creciente = 2
            esPrimo = True
            while esPrimo and creciente < i:
                if i % creciente == 0:
                    esPrimo = False
                else:
                    creciente += 1
            if esPrimo:
                print(i, " es primo")
    else:
        print("El numero que usted ingreso no es correcto. Intentelo de nuevo.")
except ValueError:
    print("ERROR\nIngrese solo numeros")
