#   Version 4
#   Calcular si un numero es primo o no
#   autor   =   "Michael Vasquez"
#   email   =   "michael.vasquez@unl.edu.ec"


def esprimo(n):
    if n < 2:
        return False

    for i in range (2, n):
        if n % i == 0:
            return False
    return True

try:
    a = int(input("Ingresa el numero que deseas saber si es primo o no:\t"))
    if a > 0:
        print(esprimo(a))
    else:
        print("Ingrese un numero positivo")

except ValueError:
    print("ERROR\nIngrese solo numeros positivos enteros")
